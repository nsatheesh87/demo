<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ParserController extends Controller
{
  protected $str;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('parser.form');
    }

    /**
     * Process the string
     *
     * @return array
     */
    public function process(Request $request)
    {
      $this->str = $request->title;
      $parsedOut = $this->parseString();
      return view('parser.form', compact('parsedOut'));
    }
    /**
     * Return the unique char array
     *
     * @return array
     */
    private function graph()
    {
      return array_unique(str_split($this->str));
    }

    /**
     * Return the parsed string
     *
     * @return array
     */
    private function parseString()
    {
      $parse = [];

      foreach($this->graph() as $node) {
      $row                 =[];
      $html                = $this->str;
      $needle              = $node;
      $lastPosition        = 0;
      $positions           = [];
      //Update the last position for each node
      while(($lastPosition = strpos($html, $needle, $lastPosition))!== false) {
          $positions[]     = $lastPosition;
          $lastPosition    = $lastPosition + strlen($needle);
      }

      $row[]               =$node;
      $row[]               =count($positions);
      $before              ="";
      $after               ="";
      //Iterate the string position to find the before and last char
      foreach($positions as $pos)
      {
          if($pos!=0)
          {
              $before.=$this->str[$pos-1].",";
          }
          else
          {
              $before.="none,";
          }
          if($pos!=(strlen($this->str)-1))
          {
              $after.=$this->str[$pos+1].",";
          }
          else
          {
              $after.="none,";
          }
      }

      $before    =substr($before, 0, -1);
      $after    =substr($after, 0, -1);
      $diff   =[];
      for($i=0;$i<(count($positions)-1);$i++)
      {
          $diff[]=(abs($positions[$i]-$positions[$i+1])-1);
      }

      $differ="";

      if(!empty($diff))
      {
          $differ=" max-distance: ".max($diff);
      }
      $row[]="before: ".$after." after: ".$before.$differ;

      $parse[]=$row;

      }

      return $parse;
    }
}
