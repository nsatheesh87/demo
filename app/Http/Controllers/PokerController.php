<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Combination;
class PokerController extends Controller
{
  protected $deck;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->deck = array(
           'values' => array('2','3','4','5','6','7','8','9','T','J','Q','K','A'),
           'suits' => array('H','D','C','S')
       );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('poker');
    }

    /**
     * Process the card input
     *
     * @return decimal
     */
    public function process(Request $request, Combination $combination)
    {
      $crossproduct = self::build($this->deck);
      $crossproductRefined = array();

      foreach ($crossproduct as $key => $value) {
        $crossproductRefined[] = $value[0].$value[1];
       }

      $data = $request->chosenCards;

      $input = array_unique(explode(",", $data));
      $visible = $input;

      $table = $input;
      $deck = array_diff($crossproductRefined, $visible);//$final - $visible

      $hand = $this->bestHand($visible, $combination);
      $wins = 0.0;
      $total = 0.0;


      foreach($deck as &$c1){
          foreach($deck as &$c2){
              if($c1 == $c2) break;
              $newtable = $table;
              array_push( $newtable, $c1);
              array_push( $newtable, $c2);

              $otherHand = $this->bestHand($newtable, $combination);
              if( $this->compare($hand, $otherHand) ){  $wins = $wins + 1.0;  }
              $total = $total + 1.0;
          }
      }


      $output =  round(($wins / $total)*100, 2);
      return view('poker', compact('output'));
    }

    /**
     * buildcard set 
     *
     * @return array
     */

    public static function build($set)
    {
        if (!$set) {
            return array(array());
        }
        $subset = array_shift($set);
        $cartesianSubset = self::build($set);
        $result = array();
        foreach ($subset as $value) {
            foreach ($cartesianSubset as $p) {
                array_unshift($p, $value);
                $result[] = $p;
            }
        }
        return $result;
    }

    /**
     * Return the value from the deck
     *
     * @return array
     */

    public function value($c)
    {
        return array_search($c[0],$this->deck['values']);
    }

    /**
     * Return the suit
     *
     * @return void|array value
     */

    public function suit($c)
    {
        return array_search($c[1],$this->deck['suits']);
    }

    /**
     * Return the amounts
     *
     * @return int
     */

     public function amounts($h)
     {
        $vals = array_map(array($this, 'value'),$h);
        $nvals = array();
        foreach($vals as $x){
            array_push(count($x));
        }
        return $nvals;
     }

     /**
      * Return the score
      *
      * @return int
      */
    public function score($h){
        $vals = array_map(array($this, 'value'),$h);
        sort($vals);
        $len=13;
        foreach ($vals as $i => $v) {
           $final =  $len ** $i * $v;
        }
        return $final;
    }

    /**
     * Return the straightFlush
     *
     * @return void
     */
    public function straightFlush($h)
    {
      $f2 = $this->flushh($h);
      $f1 = $this->straight($h);
      if($f1 && $f2)
         return true;
      else
         return false;
    }

    /**
     * Return the four of kind process
     *
     * @return void
     */

    public function fourOfAKind($h)
    {
        return in_array(4, $h);
    }

    /**
     * Return the full house
     *
     * @return void
     */

    public function fullHouse($h)
    {
        return in_array(3, $h) && in_array(2, $h);
    }

    /**
     * Return the flush header
     *
     * @return int
     */
    public function flushh($h)
    {
        return (sizeof((array_map(array($this, 'suit'),$h))) == 1);
    }

    /**
     * Return the straight
     *
     * @return void
     */
    public function straight($h)
    {
        $vals = array_map(array($this, 'value'),$h);
        sort($vals);


        if($vals[0] == 0 && $vals[count($vals)-1] == count($this->deck['values'])-1){
            array_pop($vals);
        }

        $xvals = $yvals = $vals;

        array_pop($xvals);
        array_shift($yvals);

        $vals2 = array_map(function($x){return $x+1;}, $xvals);


        $d = array_diff($vals2, $yvals);

        return (count($d) > 0 )?false:true;
    }

    /**
     * Return the three of kind
     *
     * @return void
     */
    public function threeOfAKind($h)
    {
        return in_array(3, $h);
    }

    /**
     * Return the two pair
     *
     * @return void
     */
    public function twoPair($h)
    {
        $noOfOccurence = count(array_filter($h, function ($n) { return $n == 2; }));
        return $noOfOccurence === 2;
    }


    /**
     * Return the pair array
     *
     * @return array
     */
    public function pair($h)
    {
        return in_array(2,$h);
    }

    /**
     * Return the highcard
     *
     * @return array
     */

    public function highCard($h)
    {
        return true;
    }

    /**
     * compare two hands with all possoble winning
     *
     * @return void
     */
    public function compare($h1, $h2)
    {
        $funcs = array('straightFlush','fourOfAKind', 'fullHouse',
                       'flushh','straight','threeOfAKind','twoPair',
                       'pair','highCard');
         $result = false;
         foreach ($funcs as $f) {
             $r1 = call_user_func(array($this, $f), $h1);
             $r2 = call_user_func(array($this, $f), $h2);
             $r1=true;
             $r2 =true;
             if($r1 && !$r2){
                $result = true; break;
             }elseif($r2 && !$r1){
                $result = false; break;
             }elseif ($r1 && $r2) {
                $s1 = $this->score($h1);
                $s2 = $this->score($h2);

                if ($s1 > $s2) {
                    $result = true; break;
                }elseif ($s2>$s1) {
                    $result = true; break;
                }
             }

         }

         return $result;
    }

    /**
     * Return the unique char array
     *
     * @return array
     */
    public function bestHand($cards, $combination )
    {
        $hands =  $combination->combinations($cards, 5);
        $bestHand = $hands[0];
        foreach($hands as $h){
            if( $this->compare($h, $bestHand) ){ // compare is
                $bestHand = $h;
            }
        }
        return $bestHand;
    }

}
