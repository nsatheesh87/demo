@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">String Parser</div>

                <div class="panel-body">
                  <form action="{{route('parser.process')}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <!-- Name Form Input -->
                      <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                          <label class="col-md-3 control-label" for="title">String Input </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" max-length = "255" name="title" id="title"  value="" required/>
                          @if ($errors->has('title'))
                            <span class="help-block">
                              <strong>{{ $errors->first('title') }}</strong>
                            </span>
                          @endif
                        </div>
                      </div>

                  <div style="margin-top:40px;" class="col-sm-offset-3 col-md-9">
                      <input type="submit" class="btn btn-block btn-danger" value="Parse String" />
                  </div>
                </form>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                      @if(isset($parsedOut))

                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>Result</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($parsedOut as $parse)
                            <tr>
                              <td>{{implode(':', $parse)}}</td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>

                      @endif
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
