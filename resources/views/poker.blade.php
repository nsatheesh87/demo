@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Poker Chance Calculator</div>

                <div class="panel-body">
                  <div class='rowset'>
                      <div class="form-group">
                        <label> Select Suit </label>
                          <select name="suits" id="suits" class="form-control">
                            <option>H</option>
                            <option>D</option>
                            <option>C</option>
                            <option>S</option>
                          </select>
                        </label>
                      </div>
                      <div class="form-group">
                        <label> Select Value </label>
                        <select name="value" id="value" class="form-control">
                          '2','3','4','5','6','7','8','9','T','J','Q','K','A'
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                          <option>6</option>
                          <option>7</option>
                          <option>8</option>
                          <option>9</option>
                          <option>T</option>
                          <option>J</option>
                          <option>Q</option>
                          <option>K</option>
                          <option>A</option>
                        </select>
                      </div>
                    </div>
                        <input name="add_button" type="button" class="btn btn-draft btn-danger" value="Draft Card" />

                </div>
            </div>
  <form action="{{route('poker.process')}}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="panel panel-default">
                <div class="panel-heading">Chosen Card</div>

                <div class="panel-body">
                  <div class="row">
                    <div class="col-sm-12">
                        @if(isset($output))
                          <h4> Winning Chance is : {{$output}} </h4>
                          <a class="btn btn-warning" href="{{route('poker')}}"> Reset now </a>
                        @endif
                    </div>
                      <div class="selectedCards col-sm-12">

                      </div>
                      <input type="hidden" name="chosenCards" id="chosenCards" value="" />
                  </div>
                        <input name="add_button" type="submit" class="btn  btn-danger" value="Calculate the winning chance" />

                </div>
              </form>
            </div>
        </div>
    </div>
</div>
@endsection
