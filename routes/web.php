<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/parser', [
  'as'  => 'parser',
  'uses'=> 'ParserController@index'
]);

Route::post('/parser', [
  'as'  => 'parser.process',
  'uses'=> 'ParserController@process'
]);

Route::get('/poker', [
  'as'  => 'poker',
  'uses'=> 'PokerController@index'
]);

Route::post('/poker', [
  'as'  => 'poker.process',
  'uses'=> 'PokerController@process'
]);
